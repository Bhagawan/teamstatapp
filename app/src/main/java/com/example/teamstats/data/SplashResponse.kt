package com.example.teamstats.data

import androidx.annotation.Keep

@Keep
data class SplashResponse(val url : String)

