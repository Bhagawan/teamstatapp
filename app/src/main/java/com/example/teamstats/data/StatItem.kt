package com.example.teamstats.data

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class StatItem(@SerializedName("date") val date : String,
                    @SerializedName("games") val games : Int,
                    @SerializedName("won") val won : Int,
                    @SerializedName("draw") val draw : Int,
                    @SerializedName("lost") val lost : Int,
                    @SerializedName("goals_scored") val goals_scored : Int,
                    @SerializedName("goals_conceded") val goals_conceded : Int,
                    @SerializedName("yellow_cards") val yellow_cards : Int,
                    @SerializedName("red_cards") val red_cards : Int)
