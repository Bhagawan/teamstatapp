package com.example.teamstats

import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.annotation.Keep
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import com.example.teamstats.data.StatItem
import com.example.teamstats.mvp.MainPresenter
import com.example.teamstats.mvp.MainPresenterViewInterface
import com.example.teamstats.util.SpinnerUtil.Companion.avoidDropdownFocus
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import moxy.MvpAppCompatActivity
import moxy.presenter.InjectPresenter

@Keep
class MainActivity : MvpAppCompatActivity(), MainPresenterViewInterface {
    private lateinit var currentStat : String
    private lateinit var currentLeague : String
    private lateinit var target : Target

    @InjectPresenter
    lateinit var mPresenter : MainPresenter

    override fun onBackPressed() {
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        fullscreen()
        setBackground()
        initStatSpinner()
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        fullscreen()
    }

    override fun fillLeagues(leagues: List<String>) {
        val leaguesSpinner : androidx.appcompat.widget.AppCompatSpinner = findViewById(R.id.spinner_league)
        leaguesSpinner.avoidDropdownFocus()
        val adapter = ArrayAdapter(this, R.layout.item_spinner, leagues)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        leaguesSpinner.adapter = adapter

        leaguesSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                mPresenter.setLeague(leagues[p2])
                currentLeague = leagues[p2]
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {}
        }
        leaguesSpinner.setSelection(0, false)
    }

    override fun fillGeneralStat(stat: List<StatItem>) {
        var games = 0
        var won = 0
        var lost = 0
        var draw = 0
        var scored = 0
        var conceded = 0
        var yellow = 0
        var red = 0
        for(item in stat) {
            games += (item.games)
            won += item.won
            lost += item.lost
            draw += item.draw
            scored += item.goals_scored
            conceded += item.goals_conceded
            yellow += item.yellow_cards
            red += item.red_cards
        }

        findViewById<TextView>(R.id.text_stat_games_value).text = games.toString()
        findViewById<TextView>(R.id.text_stat_win_value).text = won.toString()
        findViewById<TextView>(R.id.text_stat_draw_value).text = draw.toString()
        findViewById<TextView>(R.id.text_stat_lost_value).text = lost.toString()
        findViewById<TextView>(R.id.text_stat_scored_value).text = scored.toString()
        findViewById<TextView>(R.id.text_stat_conceded_value).text = conceded.toString()
        findViewById<TextView>(R.id.text_stat_yellow_value).text = yellow.toString()
        findViewById<TextView>(R.id.text_stat_red_value).text = red.toString()

    }

    override fun fillChart(stat: List<StatItem>) {
        val chartLayout: LinearLayout = findViewById(R.id.layout_chart)
        chartLayout.removeAllViews()

        val lineChart = LineChart(this)
        lineChart.setNoDataText("")
        val xLabels = ArrayList<String>()

        when(currentStat) {
            getString(R.string.string_stat_games) -> {
                val winEntries = ArrayList<Entry>()
                val lostEntries = ArrayList<Entry>()
                val drawEntries = ArrayList<Entry>()
                for(n in stat.size - 1 downTo 0) {
                    winEntries.add(Entry((stat.size - 1 - n).toFloat(), stat[n].won.toFloat()))
                    lostEntries.add(Entry((stat.size - 1 - n).toFloat(), stat[n].lost.toFloat()))
                    drawEntries.add(Entry((stat.size - 1 - n).toFloat(), stat[n].draw.toFloat()))
                    xLabels.add(stat[n].date)
                }
                val wonDataSet = LineDataSet(winEntries, getString(R.string.msg_win))
                wonDataSet.color = Color.GREEN
                wonDataSet.axisDependency = YAxis.AxisDependency.LEFT
                wonDataSet.setDrawValues(false)
                wonDataSet.setDrawCircles(false)
                val lostDataSet = LineDataSet(lostEntries, getString(R.string.msg_lost))
                lostDataSet.color = Color.RED
                lostDataSet.axisDependency = YAxis.AxisDependency.LEFT
                lostDataSet.setDrawValues(false)
                lostDataSet.setDrawCircles(false)
                val drawDataSet = LineDataSet(drawEntries, getString(R.string.msg_draw))
                drawDataSet.color = Color.GRAY
                drawDataSet.axisDependency = YAxis.AxisDependency.LEFT
                drawDataSet.setDrawValues(false)
                drawDataSet.setDrawCircles(false)
                val list = ArrayList<ILineDataSet>()
                list.add(wonDataSet)
                list.add(lostDataSet)
                list.add(drawDataSet)
                val lineData  = LineData(list)
                lineChart.data = lineData
            }
            getString(R.string.string_stat_goals) -> {
                val scoredEntries = ArrayList<Entry>()
                val concededEntries = ArrayList<Entry>()
                for(n in stat.size - 1 downTo 0) {
                    scoredEntries.add(Entry((stat.size - 1 - n).toFloat(), stat[n].goals_scored.toFloat()))
                    concededEntries.add(Entry((stat.size - 1 - n).toFloat(), stat[n].goals_conceded.toFloat()))
                    xLabels.add(stat[n].date)
                }
                val scoredDataSet = LineDataSet(scoredEntries, getString(R.string.msg_goals_scored))
                scoredDataSet.color = Color.GREEN
                scoredDataSet.axisDependency = YAxis.AxisDependency.LEFT
                scoredDataSet.setDrawValues(false)
                scoredDataSet.setDrawCircles(false)
                val concededDataSet = LineDataSet(concededEntries, getString(R.string.msg_goals_conceded))
                concededDataSet.color = Color.RED
                concededDataSet.axisDependency = YAxis.AxisDependency.LEFT
                concededDataSet.setDrawValues(false)
                concededDataSet.setDrawCircles(false)
                val list = ArrayList<ILineDataSet>()
                list.add(scoredDataSet)
                list.add(concededDataSet)
                val lineData  = LineData(list)
                lineChart.data = lineData
            }
            getString(R.string.string_stat_cards) -> {
                val yellowEntries = ArrayList<Entry>()
                val redEntries = ArrayList<Entry>()
                for(n in stat.size - 1 downTo 0) {
                    yellowEntries.add(Entry((stat.size - 1 - n).toFloat(), stat[n]. yellow_cards.toFloat()))
                    redEntries.add(Entry((stat.size - 1 - n).toFloat(), stat[n].red_cards.toFloat()))
                    xLabels.add(stat[n].date)
                }
                val yellowDataSet = LineDataSet(yellowEntries, getString(R.string.msg_yellow_cards))
                yellowDataSet.color = Color.YELLOW
                yellowDataSet.axisDependency = YAxis.AxisDependency.LEFT
                yellowDataSet.setDrawValues(false)
                yellowDataSet.setDrawCircles(false)
                val redDataSet = LineDataSet(redEntries, getString(R.string.msg_red_cards))
                redDataSet.color = Color.RED
                redDataSet.axisDependency = YAxis.AxisDependency.LEFT
                redDataSet.setDrawValues(false)
                redDataSet.setDrawCircles(false)
                val list = ArrayList<ILineDataSet>()
                list.add(yellowDataSet)
                list.add(redDataSet)
                val lineData  = LineData(list)
                lineChart.data = lineData
            }
        }
        val formatter: ValueFormatter = object : ValueFormatter() {
            override fun getAxisLabel(value: Float, axis: AxisBase): String {
                return xLabels[value.toInt()].takeLast(4)
            }
        }
        val xAxis = lineChart.xAxis
        xAxis.granularity = 5f
        xAxis.valueFormatter = formatter

        val legend = lineChart.legend
        legend.form = Legend.LegendForm.CIRCLE
        legend.orientation = Legend.LegendOrientation.HORIZONTAL
        legend.horizontalAlignment = Legend.LegendHorizontalAlignment.CENTER

        val desc = Description()
        desc.text = ""
        lineChart.description = desc

        lineChart.invalidate()

        chartLayout.addView(lineChart, 0, LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 400))
    }

    override fun showError(errorCode: Byte) = Toast.makeText(this, when(errorCode) {
        MainPresenter.SERVER_ERROR -> getString(R.string.msg_error_server)
        else -> getString(R.string.msg_error_default)
    }, Toast.LENGTH_SHORT).show()

    private fun fullscreen() {
        WindowInsetsControllerCompat(window, window.decorView).let { controller ->
            controller.systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
            controller.hide(WindowInsetsCompat.Type.systemBars())
        }
        WindowCompat.setDecorFitsSystemWindows(window, false)
    }

    private fun initStatSpinner() {
        val statList = ArrayList<String>()
        statList.add(resources.getString(R.string.string_stat_games))
        statList.add(resources.getString(R.string.string_stat_goals))
        statList.add(resources.getString(R.string.string_stat_cards))

        currentStat = getString(R.string.string_stat_games)
        currentLeague = ""

        val statSpinner : androidx.appcompat.widget.AppCompatSpinner = findViewById(R.id.spinner_stat)
        statSpinner.avoidDropdownFocus()
        val adapter = ArrayAdapter(this, R.layout.item_spinner, statList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        statSpinner.adapter = adapter
        statSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                currentStat = statList[p2]
                mPresenter.fillChart(currentLeague)
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {}
        }
        statSpinner.setSelection(0, false)
    }

    private fun setBackground() {
        val backLayout = findViewById<ConstraintLayout>(R.id.layout_main)
        val logo : ImageView = findViewById(R.id.img_logo)
        target = object : Target {
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                backLayout.background = BitmapDrawable(resources, bitmap)
            }
            override fun onBitmapFailed(e: java.lang.Exception?, errorDrawable: Drawable?) {}
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}
        }
        Picasso.get().load("http://195.201.125.8/TeamStatsApp/background.png").into(target)
        Picasso.get().load("http://195.201.125.8/TeamStatsApp/logo.png").resize(100, 100).into(logo)

    }
}