package com.example.teamstats.mvp

import com.example.teamstats.data.StatItem
import com.example.teamstats.util.ServerClient
import moxy.InjectViewState
import moxy.MvpPresenter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@InjectViewState
class MainPresenter : MvpPresenter<MainPresenterViewInterface>() {
    companion object{
        const val SERVER_ERROR : Byte = 0
    }
    private val leagues = HashMap<String, List<StatItem>>()
    private var loaded : Byte = 0

    override fun onFirstViewAttach() {
        ServerClient.create().getLeagueStat("premier_league.json").enqueue(object :
            Callback<List<StatItem>>{
            override fun onResponse(
                call: Call<List<StatItem>>,
                response: Response<List<StatItem>>
            ) {
                loaded++
                response.body()?.let{
                    leagues.put("Премьер Лига", it)
                }
                if(loaded > 1) fillLeagueSpinner()
            }

            override fun onFailure(call: Call<List<StatItem>>, t: Throwable) {
                viewState.showError(SERVER_ERROR)
            }

        })

        ServerClient.create().getLeagueStat("Spain_Primera.json").enqueue(object :
            Callback<List<StatItem>>{
            override fun onResponse(
                call: Call<List<StatItem>>,
                response: Response<List<StatItem>>
            ) {
                loaded++
                response.body()?.let{
                    leagues.put("Испания Примера", it)
                }
                if(loaded > 1) fillLeagueSpinner()
            }

            override fun onFailure(call: Call<List<StatItem>>, t: Throwable) {
                viewState.showError(SERVER_ERROR)
            }

        })
    }

    fun setLeague(league : String) {
        leagues[league]?.let {
            viewState.fillChart(it)
            viewState.fillGeneralStat(it)
        }
    }
    fun fillChart(league : String) {
        leagues[league]?.let { viewState.fillChart(it) }

    }

    private fun fillLeagueSpinner() {
        viewState.fillLeagues(leagues.keys.toList())
    }

}