package com.example.teamstats.mvp

import com.example.teamstats.data.StatItem
import moxy.MvpView
import moxy.viewstate.strategy.alias.OneExecution
import moxy.viewstate.strategy.alias.SingleState

interface MainPresenterViewInterface : MvpView {

    @SingleState
    fun fillLeagues(leagues : List<String>)

    @SingleState
    fun fillGeneralStat(stat : List<StatItem>)

    @SingleState
    fun fillChart(stat : List<StatItem>)

    @OneExecution
    fun showError(errorCode : Byte)
}