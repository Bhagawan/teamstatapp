package com.example.teamstats.util

import com.example.teamstats.data.SplashResponse
import com.example.teamstats.data.StatItem
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface ServerClient {

    @FormUrlEncoded
    @POST("RssNewsApp/splash.php")
    fun getSplash(@Field("locale") locale: String): Call<SplashResponse>

    @GET("TeamStatsApp/{filename}")
    fun getLeagueStat(@Path("filename") filename : String): Call<List<StatItem>>

    companion object {
        fun create() : ServerClient {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://195.201.125.8/")
                .build()
            return retrofit.create(ServerClient::class.java)
        }
    }

}